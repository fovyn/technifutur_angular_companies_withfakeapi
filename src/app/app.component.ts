import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'TechnifuturCompaniesWithMockup';

  constructor(private httpClient: HttpClient) {}

  ngOnInit(): void {
    this.httpClient.get('/api/stagiaire').subscribe(data => console.log(data));
  }


}
